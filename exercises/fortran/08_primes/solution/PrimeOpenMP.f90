module modu
use omp_lib
implicit none
!save
integer :: gprogress = 0, gprimesfound = 0, clstart, clend
integer(kind=8), dimension(1000000) :: globalPrimes

contains

subroutine getclargs(CLstart, CLend)
integer,intent(out) :: CLstart, CLend
CHARACTER(LEN=256) :: argument

if (iargc() == 2) then
    CALL GETARG(1, argument); READ (argument, *) CLstart
    CALL GETARG(2, argument); READ (argument, *) CLend
    !write(*,*) "Argumetz: ", CLstart, CLend
    !CLstart = atoi(argv[1]);
    !CLend   = atoi(argv[2]);
else
	write(*,*)'Usage:-  primes <start range> <end range>'
	stop
end if

if (clstart > clend) then
	write(*,*)'Start value must be less than or equal to end.'
	stop
end if	

if ( (clstart < 1) .and. (clend < 2)) then
	write (*,*) 'Range must be positive integers'
	stop
end if

!	*start = CLstart;
!	*end = CLend;
!if (clstart < 2) *start = 2;
!if (*start <= 2) globalPrimes[gPrimesFound++] = 2;
if (CLstart <  2 ) CLstart = 2
if (CLstart <= 2 ) globalPrimes(gPrimesFound) = 2; gPrimesFound = gPrimesFound + 1;
end subroutine getclargs


subroutine showprogr (sval, srange)
integer, intent(in) :: sval, srange 
integer :: percentdone = 0
!$omp critical
gprogress = gprogress + 1
percentdone = 0.5 + 200. * gprogress/srange
!$omp end critical
if(mod(percentdone,10)==0) then
	write(*,*) percentdone, '%'
	!write(*,'(i,a,$)') percentdone, '%'
end if
end subroutine showprogr


subroutine findprimes (fstart,fend)
integer, intent(in) :: fstart, fend
integer :: i, frange

frange = fend - fstart + 1

!$omp parallel do 
do i=fstart, fend, 2
	if(testforprime(i)) then
		!$omp critical
		globalPrimes(gPrimesFound) = i 
		gPrimesFound = gPrimesFound + 1
		!$omp end critical
	end if
        !call showprogr(i, frange)
end do
end subroutine findprimes


function testforprime(val)
	logical :: testforprime
	integer, intent(in) :: val
	integer :: limit, factor
        factor = 3
	limit = 0.5 + sqrt(1.*val)
        do while ((factor <= limit) .and. (mod(val, factor)/=0)) 
          factor = factor + 1
        enddo
	if (factor .gt. limit) then
		testforprime = .true. 
	else
		testforprime = .false.
	end if
end function testforprime


end module modu


program main
use modu
use omp_lib
implicit none
integer :: pstart, pend, argc, argv, start
double precision :: before, after

call getclargs(pstart, pend)		! siehe zeile 15

        start=3
if ( mod(pstart,2) == 0 ) then
	start = start + 1	!ensure we start with an odd number
end if
if ( pstart < 3 ) then
        start = 3
endif
before = omp_get_wtime()
 call findprimes( start, pend )
after =  omp_get_wtime()
write(*,*) " ";  write (*,*) gprimesfound, 'primes found between ', pstart, 'and', pend, 'in', after-before, ' seconds.'	  
end program main




























